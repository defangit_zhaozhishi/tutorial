package cn.devmgr.tutorial;

import java.net.UnknownHostException;
import java.security.Principal;

import cn.devmgr.tutorial.utils.JiVariantUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jinterop.dcom.common.JIException;
import org.jinterop.dcom.core.IJIUnsigned;
import org.jinterop.dcom.core.JIString;
import org.jinterop.dcom.core.JIVariant;
import org.openscada.opc.lib.common.NotConnectedException;
import org.openscada.opc.lib.da.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@Controller
public class SocketController {
    private final static Log log = LogFactory.getLog(SocketController.class);

    private String test;

    private String itemMessage;

    @Autowired
    OPCService opcService;


    @Autowired
    private SimpMessagingTemplate template;


    public static void stopThreadByName(String name){
        for (Thread t:Thread.getAllStackTraces().keySet()){
            if (t.getName().equals(name)){
                t.stop();
            }
        }

    }

    /**
     * SendTo SendToUser两个注解必须和 MessageMapping一起用在有作用；
     * SendToUser只是发给消息来源用户的订阅队列里； SendTo则发送到所有用户的订阅队列里
     *
     * 不写SendTo/SendToUser注解，只写MessageMapping注解，也能接收客户端发送过来的消息。
     * SendTo注解的作用是给客户端发消息（发送到订阅队列里，不是回接收的客户端的消息，STOMP中无回消息的概念）
     *
     * MessageMapping注解中配置的接收地址和WebScoketConfig中setApplicationDestinationPrefixes()设置的地址前缀
     * 一起构成了客户端向服务器端发送消息时使用地址
     */
    @MessageMapping("/all")
    @SendTo("/topic/clockmessage")
    public ClockMessage toAll(ClockMessage message, Principal principal) throws Exception {
        if(log.isTraceEnabled()) {
            log.trace("toAll(接受到消息)" + message);
        }
        Thread.sleep(100);
        //这个方法也能发
        this.template.convertAndSend("/topic/clockmessage",  new ClockMessage("Hello, from controller now!"));

        //由于使用注解@SendTo，返回结果也会被convertAndSend
        return new ClockMessage("toAll, 来自"  + principal.getName() + "的消息：" + message.getMessage() + " ");
    }


    @MessageMapping("/one")
    @SendToUser("/topic/clockmessage")
    public void toOne(ClockMessage message, Principal principal) throws Exception {
        itemMessage = message.getMessage();
        //由于增加了SendToUser注解，返回结果会被convertAndSend给特定用户，调用这个方法发消息的用户principal中的用户
        Server server = opcService.openServer();
        String result = "ItemID"+message.getMessage()+"测定值：";
        stopThreadByName("UtgardSyncReader");
        try {
            // 连接到服务
            // add sync access, poll every 500 ms，启动一个同步的access用来读取地址上的值，线程池每500ms读值一次
            final AccessBase access =  new SyncAccess(server,500);
            // 这是个回调函数，就是读到值后执行这个打印，是用匿名类写的，当然也可以写到外面去
            access.unbind();
            access.addItem(message.getMessage(), new DataCallback() {
                public void changed(Item item, ItemState itemState) {
                    try {
                        log.debug("监控项的数据类型是：-----" + itemState.getValue().getType());
                        log.debug("监控项的时间戳是：-----" + itemState.getTimestamp().getTime());
                        log.debug("监控项的详细信息是：-----" + itemState.getValue());
                        log.debug("监控项的点位名称是：------" + itemMessage);
                         // 类型实际是数字，用常量定义的
                        test = JiVariantUtil.parseVariant(itemState.getValue())+"";
                    } catch (JIException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (itemMessage.equals(message.getMessage())){
                        //template.convertAndSendToUser(principal.getName(),"/topic/clockmessage",new ClockMessage(result+test));
                        template.convertAndSendToUser(principal.getName(),"/topic/clockmessage",new ClockMessage(test));
                    }
                }
            });
            // start reading，开始读值
            access.bind();
            // wait a little bit，有个10秒延时
            Thread.sleep(10 * 1000);
            // stop reading，停止读取
           // access.unbind();

        } catch (final JIException e) {
            System.out.println(String.format("%08X: %s", e.getErrorCode(), server.getErrorMessage(e.getErrorCode())));
        }
        //return new ClockMessage("toOne, 来自"  + principal.getName() + "的消息：" + message.getMessage() + " ");
    }

}
