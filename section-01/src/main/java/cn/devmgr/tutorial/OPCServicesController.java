package cn.devmgr.tutorial;

import org.jinterop.dcom.common.JIException;
import org.jinterop.dcom.core.JIString;
import org.jinterop.dcom.core.JIVariant;
import org.openscada.opc.lib.common.AlreadyConnectedException;
import org.openscada.opc.lib.common.ConnectionInformation;
import org.openscada.opc.lib.common.NotConnectedException;
import org.openscada.opc.lib.da.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.UnknownHostException;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/opc")
public class OPCServicesController {

    @Autowired
    OPCConfig opcConfig;

    @GetMapping("items")
    public List<String> getAll() {
        final ConnectionInformation ci = new ConnectionInformation();
        ci.setHost(opcConfig.getHost());         // 电脑IP
        ci.setDomain("");                  // 域，为空就行
        ci.setUser(opcConfig.getUser());             // 电脑上自己建好的用户名
        ci.setPassword(opcConfig.getPassword());          // 用户名的密码
        ci.setHost("192.168.9.158");         // 电脑IP
        ci.setDomain("");                  // 域，为空就行
        ci.setUser("OPCServer");             // 电脑上自己建好的用户名
        //ci.setPassword("123456");
        // 使用MatrikonOPC Server的配置
        // ci.setClsid("F8582CF2-88FB-11D0-B850-00C0F0104305"); // MatrikonOPC的注册表ID，可以在“组件服务”里看到
        // final String itemId = "u.u";    // 项的名字按实际

        // 使用KEPServer的配置
        ci.setClsid(opcConfig.getClsId()); // KEPServer的注册表ID，可以在“组件服务”里看到
       // ci.setClsid("7BC0CC8E-482C-47CA-ABDC-0FE7F9C6E729");
        //final String itemId = "u.u.u";    // 项的名字按实际，没有实际PLC，用的模拟器：simulator
        //final String itemId = "通道 1.设备 1.标记 1";

        // 启动服务
        final Server server = new Server(ci, Executors.newSingleThreadScheduledExecutor());

        try {
            // 连接到服务
            server.connect();
            Collection<String> itemIds = server.getFlatBrowser().browse();
            List<String> result = itemIds.parallelStream().collect(Collectors.toList());
            // add sync access, poll every 500 ms，启动一个同步的access用来读取地址上的值，线程池每500ms读值一次
            // 这个是用来循环读值的，只读一次值不用这样
            return result;
        } catch (final JIException e) {
            System.out.println(String.format("%08X: %s", e.getErrorCode(), server.getErrorMessage(e.getErrorCode())));
        } catch (AlreadyConnectedException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }
}
