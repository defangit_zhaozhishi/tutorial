package cn.devmgr.tutorial;

import java.net.UnknownHostException;
import java.nio.channels.AlreadyConnectedException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {
    private Log log = LogFactory.getLog(ScheduledTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    private String test;
    @Autowired
    OPCService opcService;


    @Autowired
    private SimpMessagingTemplate template;

//    @Scheduled(fixedRate = 2000)
//    public void reportCurrentTime() throws Exception{
//        if(log.isTraceEnabled()) {
//            log.trace("current time is " + dateFormat.format(new Date()));
//        }
//        //这个消息是发送给订阅了/topic/clockmessage的所有用户
//
//        Server server = opcService.getServer();
//        IJIUnsigned value;
//        String result = "ItemID"+"通道 1.设备 1.标记 1";
//        try {
//            Group group = server.addGroup();
//            Item item = group.addItem("通道 1.设备 1.标记 1");
//            ItemState itemState = item.read(false);
//            if (itemState!=null){
//                if(itemState.getValue().getType() == JIVariant.VT_UI2) {  // 字符的类型是18
//                    try {
//                        value = itemState.getValue().getObjectAsUnsigned();
//                        System.out.println("-----unsigned Char类型值： " + value.getValue().shortValue());
//                        result+="opc res:"+ value.getValue().shortValue();
//                        System.out.println(result);
//                    } catch (JIException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//            } catch (Exception e) {
//            log.error("同步读取失败！", e);
//        }
//        this.template.convertAndSend("/topic/clockmessage",new ClockMessage(result));
//    }

//    @Scheduled(fixedRate = 30000)
//    public void reportCurrentTime() throws Exception{
//        if(log.isTraceEnabled()) {
//            log.trace("current time is " + dateFormat.format(new Date()));
//        }
//        //这个消息是发送给订阅了/topic/clockmessage的所有用户
//
//        Server server = opcService.getServer();
//        IJIUnsigned value;
//        String result = "ItemID"+"通道 1.设备 1.标记 1";
//
//        try {
//            // 连接到服务
//            // add sync access, poll every 500 ms，启动一个同步的access用来读取地址上的值，线程池每500ms读值一次
//            // 这个是用来循环读值的，只读一次值不用这样
//            final AccessBase access = new SyncAccess(server, 100);
//            // 这是个回调函数，就是读到值后执行这个打印，是用匿名类写的，当然也可以写到外面去
//            access.addItem("通道 1.设备 1.标记 1", new DataCallback() {
//                public void changed(Item item, ItemState itemState) {
//                    int type = 0;
//                    try {
//                        type = itemState.getValue().getType(); // 类型实际是数字，用常量定义的
//                    } catch (JIException e) {
//                        e.printStackTrace();
//                    }
//                    System.out.println("监控项的数据类型是：-----" + type);
//                    System.out.println("监控项的时间戳是：-----" + itemState.getTimestamp().getTime());
//                    System.out.println("监控项的详细信息是：-----" + itemState.getValue());
//
//                    // 如果读到是short类型的值
//                    if (type == JIVariant.VT_I2) {
//                        short n = 0;
//                        try {
//                            n = itemState.getValue().getObjectAsShort();
//                        } catch (JIException e) {
//                            e.printStackTrace();
//                        }
//                        System.out.println("-----short类型值： " + n);
//                    }
//
//                    // 如果读到是字符串类型的值
//                    if(type == JIVariant.VT_BSTR) {  // 字符串的类型是8
//                        JIString value = null;
//                        try {
//                            value = itemState.getValue().getObjectAsString();
//                        } catch (JIException e) {
//                            e.printStackTrace();
//                        } // 按字符串读取
//                        String str = value.getString(); // 得到字符串
//                        System.out.println("-----String类型值： " + str);
//                    }
//                    if(type == JIVariant.VT_UI2) {  // 字符的类型是18
//                        IJIUnsigned value ;
//                        try {
//                            value = itemState.getValue().getObjectAsUnsigned();
//                            System.out.println("-----unsigned Char类型值： " + value.getValue().shortValue());
//                            test = value.getValue().shortValue()+"";
//                        } catch (JIException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    template.convertAndSend("/topic/clockmessage",new ClockMessage(result+test));
//                }
//            });
//            // start reading，开始读值
//            access.bind();
//            // wait a little bit，有个10秒延时
//            Thread.sleep(4 * 1000);
//            // stop reading，停止读取
//            //access.unbind();
//        } catch (final JIException e) {
//            System.out.println(String.format("%08X: %s", e.getErrorCode(), server.getErrorMessage(e.getErrorCode())));
//        }
//
//    }


//    @Scheduled(fixedRate = 10000)
//    public void reportCurrentTimeTo000() throws Exception{
//        if(log.isTraceEnabled()) {
//            log.trace("reportCurrentTimeTo000: current time is " + dateFormat.format(new Date()));
//        }
//        //这个消息只有Principal.getName()为000用户才收到
//        this.template.convertAndSendToUser("000", "/topic/clockmessage",  new ClockMessage("Konichiwa 000, it's " + dateFormat.format(new Date()) + " now!"));
//    }
}
